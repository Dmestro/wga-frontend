import logo from './logo.svg';
import {
  Button, List, PageHeader, Avatar
} from 'antd';
import './App.css';

// import links from "./data/links";
import nodes from "./data/nodes";
import links from "./data/links";
import Graph from 'react-graph-network';
import Layout, { Content, Header } from 'antd/lib/layout/layout';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Analyze from "./components/analyze/Analyze"
import MainPage from './components/man-page/MainPage';


const data = {
  nodes: nodes,
  links: links
};


const myConfig = {
  nodeHighlightBehavior: true,
  node: {
    color: "lightgreen",
    size: 120,
    highlightStrokeColor: "blue",
  },
  link: {
    highlightColor: "lightblue",
  },
};

const onClickNode = function (nodeId) {
  window.alert(`Clicked node ${nodeId}`);
};

const onClickLink = function (source, target) {
  window.alert(`Clicked link between ${source} and ${target}`);
};


function App() {
  return (
    <div className="App">
      <PageHeader
        className="site-page-header"
        title="Webgraph analyzer"
      />
      <Router>
        <Switch>
          <Route path='/analyze/:id' component={Analyze} />
          <Route path='/' component={MainPage} />
        </Switch>
      </Router>


    </div>


  );
}

export default App;
