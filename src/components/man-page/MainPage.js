import React from "react"

import "./MainPage.css"
import {
    Button, List, Modal, Input
  } from 'antd';

import { UploadOutlined } from '@ant-design/icons';

  const dataList = [
    {
      title: 'Google contest network',
      description: 'Large ananymized network dataset from Google contest'
    },
    {
      title: 'Cambridge net',
      description: 'Real network topology of Cambirdge campus'
    }
  ];

const { TextArea } = Input;

class MainPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {isModalOpened: false}

        this.toggleModal = this.toggleModal.bind(this)
    }

    toggleModal() {
        this.setState({isModalOpened: !this.state.isModalOpened})
    }

    render() {
      return (
        <div className="graphs-list">
        <h1>Выберите граф</h1>
        <List
          itemLayout="horizontal"
          dataSource={dataList}
          renderItem={item => (
            <List.Item>
              <List.Item.Meta
                title={<a href={"/analyze/"+item.title}>{item.title}</a>}
                description={item.description}
              />
            </List.Item>
          )}
        />

          <Button type="primary" onClick={this.toggleModal}>Добавить граф</Button>


            <Modal title="Добавление графа" visible={this.state.isModalOpened}>

                <div className="modal-content">
                    <div>
                        <div>Название</div>
                        <Input />
                    </div>

                    <div>
                        <div>Описание</div>
                        <TextArea rows={4} />
                    </div>

                    <Button icon={<UploadOutlined />}>Добавить файлы</Button>
                </div>


            </Modal>

        {/* <Analyze></Analyze> */}
      </div>

      )
    }
  }

export default MainPage;
