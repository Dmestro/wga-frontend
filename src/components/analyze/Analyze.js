import React from "react"
import "./Analyze.css"

import nodes from "./../../data/nodes";
import links from "./../../data/links";
import Graph from 'react-graph-network';

import {LineSeries, XYPlot, XAxis, YAxis, VerticalRectSeries, Highlight} from 'react-vis';

import {Tabs} from 'antd';

const {TabPane} = Tabs;


const data = {
    nodes: nodes,
    links: links
};

const DATA = [
    {x0: 1, x: 795.0, y: 0.000981997827497957},
    {x0: 795.0, x: 1589.0, y: 0.00011938672217868998},
    {x0: 1589.0, x: 2383.0, y: 4.371908136121041e-05},
    {x0: 2383.0, x: 3177.0, y: 5.2126597007597034e-05},
    {x0: 3177.0, x: 3971.0, y: 3.1948559456269146e-05},
    {x0: 3971.0, x: 4765.0, y: 2.0178037551327884e-05},
    {x0: 4765.0, x: 5559.0, y: 1.0089018775663942e-05},
    {x0: 5559.0, x: 6353.0, y: 1.6815031292773236e-06}
];

class Analyze extends React.Component {

    constructor(props) {
        super(props);
        console.log("CALL CONSTRUCTOR");
        this.state = {id: null}
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        this.setState({id})
    }

    render() {
        return (
            <div className="analyze-container">
                <Tabs defaultActiveKey="1">
                    <TabPane tab="Информация" key="1">
                        <div className={'info'}>
                            <div className={'info-row'}>
                                <div>Название:</div>
                                <div>Cambridge Net</div>
                            </div>

                            <div className={'info-row'}>
                                <div>Описание:</div>
                                <div>Real network topology of Cambirdge campus</div>
                            </div>

                            <div className={'info-row'}>
                                <div>Количество вершин:</div>
                                <div>18960</div>
                            </div>

                            <div className={'info-row'}>
                                <div>Количество связей:</div>
                                <div>75863</div>
                            </div>

                            <div className={'info-row'}>
                                <div>Критерий соответствия Пирсона для модели Барабаши-Альберт:</div>
                                <div>8.956</div>
                            </div>

                            <div className={'info-row'}>
                                <div>Критерий соответствия Пирсона для модели Боллобоша-Риордана:</div>
                                <div>7.3208</div>
                            </div>

                            <div className={'info-row'}>
                                <div>Критерий соответствия Пирсона для модели Бакли-Остгуса:</div>
                                <div>10.3919</div>
                            </div>

                            <div className={'info-row'}>
                                <div>Наиболее соответствующая модель: </div>
                                <div>Модель Бакли-Остгуса</div>
                            </div>
                        </div>
                    </TabPane>
                    <TabPane tab="Визуализация" key="2">
                        <div className="graph-container">
                            <Graph
                                data={data}
                                zoomDepth={100}
                                nodeDistance={50}
                                enableDrag={false}
                                id="graph"/>
                        </div>

                    </TabPane>
                    <TabPane tab="Гистограмма распределения степеней вершин" key="3">
                        {/*Content of Tab Pane 3*/}

                        <div>
                            <XYPlot width={900} height={400}>
                                <XAxis />
                                <YAxis />
                                <VerticalRectSeries
                                    data={DATA}
                                    stroke="white"
                                    colorType="literal"
                                />

                                {/*<LineSeries*/}
                                {/*    data={[{x: 1, y: 15}, {x: 2, y: 8}, {x: 3, y: 1}]}*/}
                                {/*/>*/}
                            </XYPlot>
                        </div>

                    </TabPane>

                    {/*<TabPane tab="Соответствие теоретическим моделям" key="4">*/}
                    {/*    Content of Tab Pane 4*/}
                    {/*</TabPane>*/}
                </Tabs>
            </div>
        )
    }
}

export default Analyze;
