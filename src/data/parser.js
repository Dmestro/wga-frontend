const fs = require('fs');
const readline = require('readline');

async function processLineByLine() {
  const fileStream = fs.createReadStream('cambridge_net.txt');

  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
  });
  // Note: we use the crlfDelay option to recognize all instances of CR LF
  // ('\r\n') in input.txt as a single line break.

  let result = [];
  let nodes = [];

  for await (const line of rl) {
    // Each line in input.txt will be successively available here as `line`.
    console.log(`Line from file: ${line}`);

    let lineData = line.split("\t");

    let item = {
        source: lineData[0],
        target: lineData[1]
    }

    nodes.push(lineData[0]);
    nodes.push(lineData[1]);

    result.push(item)

  }

  nodes = [...new Set(nodes)].map(n => ({id: n}));

  fs.writeFile('links.js', JSON.stringify(result), function (err) {
    if (err) return console.log(err);
  });

  fs.writeFile('nodes.js', JSON.stringify(nodes), function (err) {
    if (err) return console.log(err);
  });
}

processLineByLine();