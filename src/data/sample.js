import logo from './logo.svg';
import { Button } from 'antd';
import './App.css';

// import links from "./data/links";
import nodes from "./data/nodes";
import links from "./data/links";
import Graph from 'react-graph-network';

const data = {
  nodes: nodes,
  links: links
};


const myConfig = {
  nodeHighlightBehavior: true,
  node: {
    color: "lightgreen",
    size: 120,
    highlightStrokeColor: "blue",
  },
  link: {
    highlightColor: "lightblue",
  },
};

const onClickNode = function(nodeId) {
  window.alert(`Clicked node ${nodeId}`);
};

const onClickLink = function(source, target) {
  window.alert(`Clicked link between ${source} and ${target}`);
};

function App() {

  console.log("n = ", nodes);


  return (
    <div className="App">
    
      {/* <Button type="primary">Button</Button> */}
      <div style={{ height: '100vh' }}>
        <Graph 
        data={data}
        zoomDepth={10}
        nodeDistance={30}
        enableDrag={false}
         id="graph" />
  </div>
    </div>
  );
}

export default App;
